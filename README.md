# Agenda Uml Bl At
- Pré-requis avoir java 17 et docker démarré
- Dans dossier target se trouve le .jar du projet
- Commande pour exécuter l'application:
java -jar  exoUML-0.0.1-SNAPSHOT.jar
- Pour tester se connecter à http://localhost:8080/swagger-ui/index.html#/
Puis faire des appels sur les différentes API

Le diagramme de classe UML est dans le fichier "ressources" du projet.

