package com.example.exo.repository;

import com.example.exo.model.ContactDetail;
import com.example.exo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactDetailRepository extends JpaRepository<ContactDetail, Integer> {
}
