package com.example.exo.api;

import com.example.exo.model.User;
import com.example.exo.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserApi {

    //TOOD: create DTO and Mapping to send just necessary informations

    private final UserService userService;

    public UserApi(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of users ")
    public ResponseEntity<List<User>> getAll() {

        return ResponseEntity.ok(
                this.userService.getAll()
        );
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return an user")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "404", description = " Unknown user")
    public ResponseEntity<User> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.userService.getById(id));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create an user")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<User> createUser(@RequestBody final User user) {

        try {
            this.userService.createUser(user);
            return ResponseEntity
                    .created(URI.create("/users/" + user.getId()))
                    .body(user);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
