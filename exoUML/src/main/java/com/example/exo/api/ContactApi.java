package com.example.exo.api;

import com.example.exo.model.Contact;
import com.example.exo.service.ContactService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/contacts")
public class ContactApi {

    private final ContactService contactService;

    public ContactApi(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of users ")
    public ResponseEntity<List<Contact>> getAll() {

        return ResponseEntity.ok(
                this.contactService.getAll()
        );
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a contact")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "404", description = " Unknown contact")
    public ResponseEntity<Contact> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.contactService.getById(id));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a contact")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<Contact> createContact(@RequestBody final Contact contact) {

        try {
            this.contactService.createContact(contact);
            return ResponseEntity
                    .created(URI.create("/contacts/" + contact.getId()))
                    .body(contact);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
