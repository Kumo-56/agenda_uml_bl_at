package com.example.exo.api;


import com.example.exo.model.Agenda;
import com.example.exo.service.AgendaService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/agenda")
public class AgendaApi {

    private final AgendaService agendaService;

    public AgendaApi(AgendaService agendaService) {
        this.agendaService = agendaService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of agendas ")
    public ResponseEntity<List<Agenda>> getAll() {

        return ResponseEntity.ok(
                this.agendaService.getAll()
        );
    }
}
