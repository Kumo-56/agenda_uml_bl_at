package com.example.exo.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@DiscriminatorValue("PHONE")
public class Phone extends ContactDetail {

    private String numberPhone;

    @Override
    Boolean validate() {

        Pattern pattern = Pattern.compile("^\\+?[1-9][0-9]{7,14}$");

        Matcher matcher = pattern.matcher(this.numberPhone);

        return matcher.matches();

    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) throws Exception {
        if (this.validate()) {
            this.numberPhone = numberPhone;
        } else {
            throw new Exception("wrong phone number format");
        }

    }
}
