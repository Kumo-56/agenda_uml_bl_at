package com.example.exo.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@DiscriminatorValue("MAIL")
public class Mail extends ContactDetail {

    @Column
    private String adressMail;

    @Override
    Boolean validate() {
        String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Z|a-z]{2,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(this.adressMail);

        return matcher.matches();
    }

    public String getAdressMail() {
        return adressMail;
    }

    public void setAdressMail(String adressMail) throws Exception {

        if (this.validate()) {
            this.adressMail = adressMail;
        }
        else { throw new Exception("format mail invalide");
        }
    }

}
