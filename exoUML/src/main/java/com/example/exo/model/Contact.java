package com.example.exo.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    public String name;

    @Column
    public String prenom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agenda_numero")
    private Agenda agenda;

    @OneToMany(mappedBy = "contact")
    private List<ContactDetail> contactsDetail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ContactDetail> getContactsDetail() {
        return contactsDetail;
    }

    public void setContactsDetail(List<ContactDetail> contactsDetail) {
        this.contactsDetail = contactsDetail;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }
}
