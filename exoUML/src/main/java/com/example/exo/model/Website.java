package com.example.exo.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@DiscriminatorValue("WEBSITE")
public class Website extends ContactDetail {

    private String url;

    @Override
    Boolean validate() {
        Pattern pattern = Pattern.compile("^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$");

        Matcher matcher = pattern.matcher(this.url);

        return matcher.matches();

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) throws Exception {

        if (this.validate()) {
            this.url = url;
        } else {
            throw new Exception("wrong format of url");
        }
    }
}
