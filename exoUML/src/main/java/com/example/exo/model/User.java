package com.example.exo.model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    private Integer id;

    @Column()
    public String login;

    @Column
    private String password;

    @Transient
    private String jwtToken;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
