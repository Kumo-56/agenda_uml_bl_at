package com.example.exo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExoUmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExoUmlApplication.class, args);
	}

}
