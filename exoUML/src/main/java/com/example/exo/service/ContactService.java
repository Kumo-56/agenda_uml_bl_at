package com.example.exo.service;

import com.example.exo.model.Contact;

import java.util.List;

public interface ContactService {

    Contact getById(Integer id);

    Contact createContact(Contact contact);

    Contact updateContact(Contact contact);

    List<Contact> getAll();
}
