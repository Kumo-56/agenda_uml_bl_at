package com.example.exo.service;

import com.example.exo.model.Agenda;

import java.util.List;

public interface AgendaService {

    Agenda getById(Integer id) throws Exception;

    Agenda createAgenda(Agenda agenda);

    Agenda updateAgenda(Agenda agenda) throws Exception;

    List<Agenda> getAll();

}
