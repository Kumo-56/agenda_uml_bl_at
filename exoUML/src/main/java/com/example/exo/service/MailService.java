package com.example.exo.service;

import com.example.exo.model.Mail;


public interface MailService {

    Mail getById(Integer id) throws Exception;

    Mail createMail(Mail mail);

    Mail updateMail(Mail mail) throws Exception;

    
}
