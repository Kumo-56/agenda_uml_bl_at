package com.example.exo.service.impl;

import com.example.exo.model.User;
import com.example.exo.repository.UserRepository;
import com.example.exo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    //TODO: add controle and token validation with springSecurity

    private final UserRepository userRepository;

    Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getById(Integer id) {
        log.info("Attempting to get an user by Id");
        return this.userRepository.getReferenceById(id);
    }

    @Override
    public User createUser(User user) {
        log.info("Attempting to create an user");

        return this.userRepository.save(user);
    }


    @Override
    public User updateUser(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return this.userRepository.findAll();
    }
}
