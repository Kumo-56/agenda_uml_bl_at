package com.example.exo.service.impl;

import com.example.exo.model.Contact;
import com.example.exo.repository.ContactRepository;
import com.example.exo.service.ContactService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

    private final ContactRepository contactRepository;

    public ContactServiceImpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Contact getById(Integer id) {
        return this.contactRepository.getReferenceById(id);
    }

    @Override
    public Contact createContact(Contact contact) {
        return this.contactRepository.save(contact);
    }

    @Override
    public Contact updateContact(Contact contact) {
        return this.contactRepository.save(contact);
    }

    @Override
    public List<Contact> getAll() {
        return this.contactRepository.findAll();
    }
}
