package com.example.exo.service.impl;

import com.example.exo.model.Mail;
import com.example.exo.repository.MailRepository;
import com.example.exo.service.MailService;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    private final MailRepository mailRepository;

    public MailServiceImpl(MailRepository mailRepository) {
        this.mailRepository = mailRepository;
    }

    @Override
    public Mail getById(Integer id) throws Exception {
        return mailRepository.findById(id).orElseThrow(()-> new Exception("No mail found for the given id"));
    }

    @Override
    public Mail createMail(Mail mail) {
        return this.mailRepository.save(mail);
    }

    @Override
    public Mail updateMail(Mail mail) throws Exception {
        Mail mail1 = this.getById(mail.getId());
        mail1.setAdressMail(mail1.getAdressMail());
        mail1.setContact(mail1.getContact());
        return mailRepository.save(mail1);
    }


}
