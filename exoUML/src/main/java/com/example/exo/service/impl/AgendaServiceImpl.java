package com.example.exo.service.impl;

import com.example.exo.model.Agenda;
import com.example.exo.model.Contact;
import com.example.exo.repository.AgendaRepository;
import com.example.exo.service.AgendaService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class AgendaServiceImpl implements AgendaService {

    private final AgendaRepository agendaRepository;

    public AgendaServiceImpl(AgendaRepository agendaRepository) {
        this.agendaRepository = agendaRepository;
    }

    @Override
    public Agenda getById(Integer id) throws Exception {
       return agendaRepository.findById(id).orElseThrow(() -> new Exception("No agenda found for the given ID."));
    }

    @Override
    public Agenda createAgenda(Agenda agenda) {
        return this.agendaRepository.save(agenda);
    }

    @Override
    public Agenda updateAgenda(Agenda agenda) throws Exception {
       Agenda agenda1 = this.getById(agenda.getNumero());
        agenda1.setContacts(agenda.getContacts());
        agenda1.setDenomination(agenda.getDenomination());
        return agendaRepository.save(agenda1);
    }

    @Override
    public List<Agenda> getAll() {
        return this.agendaRepository.findAll();
    }
}

