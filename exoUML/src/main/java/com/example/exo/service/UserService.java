package com.example.exo.service;

import com.example.exo.model.User;

import java.util.List;

public interface UserService {


    User getById(Integer id);

    User createUser(User user);

    User updateUser(User user);

    List<User> getAll();
}
